const onlyKeys=(testObject)=>{
    const keys=[]
    for (const key in testObject){
        keys.push(key)
    }
    return keys
}

module.exports=onlyKeys;