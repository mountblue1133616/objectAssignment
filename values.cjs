const onlyValues=(testObject)=>{
    const values=[]
    for (const key in testObject){
        values.push(testObject[key])
    }
    return values
}

module.exports=onlyValues;