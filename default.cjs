const defaults=(obj,defaultProps)=>{
    for (var prop in defaultProps){
        var propExists=false
        for (var objprop in obj){
            if(prop===objprop){
                propExists=true
            }
        }
    if (!propExists){
        obj[prop]=defaultProps[prop]
    }
    }
    return obj;
}

module.exports=defaults;