const invert=(testObject)=>{
    let inverted={}
    for (const key in testObject){
        const newKey=testObject[key];
        inverted[newKey]=key;
    }
    return inverted;
}

module.exports=invert;